#pragma once

namespace th
{
	namespace cmd
	{
		namespace internal
		{
			template<class CommandInfo, class Args, class RedoInvoker, class UndoInvoker>
			class Command
			{
			public:
				Command(CommandInfo actionInfo, Args args, RedoInvoker redo, UndoInvoker undo) :
					actionInfo_(std::move(actionInfo)),
					args_(std::move(args)),
					redo_(std::move(redo)),
					undo_(std::move(undo))
				{}

				void redo()
				{
					redo_(args_);
				}

				void undo()
				{
					undo_(args_);
				}

				CommandInfo commandInfo() const
				{
					return actionInfo_;
				}

			private:
				CommandInfo actionInfo_;
				Args args_;
				RedoInvoker redo_;
				UndoInvoker undo_;

			};
		}
	}
}
