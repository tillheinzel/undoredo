#pragma once

#pragma warning(push, 0)
#include <iostream>
#include <stack>
#include <utility>
#include <optional>

#include <range/v3/all.hpp>
#pragma warning(pop)

#include "Stack.hpp"
#include "Command.hpp"
#include "StrongType.hpp"

namespace th
{
	namespace cmd
	{
		template<class CommandInfoT, class ArgsT, class RedoInvokerT, class UndoInvokerT>
		class CommandStack
		{
			/*
			 * possible States: 
			 * - empty
			 * - at back of do -stack (after a push)
			 * - somewhere in the middle (after undos)
			 */

			static_assert(std::is_move_constructible_v<CommandInfoT>);
			static_assert(std::is_move_constructible_v<ArgsT>);
			static_assert(std::is_move_constructible_v<RedoInvokerT>);
			static_assert(std::is_move_constructible_v<UndoInvokerT>);

			static_assert(std::is_invocable_v<RedoInvokerT, ArgsT>);
			static_assert(std::is_invocable_v<UndoInvokerT, ArgsT>);


			using Command = internal::Command<CommandInfoT, ArgsT, RedoInvokerT, UndoInvokerT>;
		public:
			using CommandInfo = internal::StrongType<CommandInfoT, struct CommandInfoID>;
			using Args = internal::StrongType<ArgsT, struct ArgsID>;
			using RedoInvoker = internal::StrongType<RedoInvokerT, struct RedoInvokerID>;
			using UndoInvoker = internal::StrongType<UndoInvokerT, struct UndoInvokerID>;

			using KeepOnlyLatest = internal::StrongType<std::size_t, struct KeepOnlyLatestID>;
			/*
			 * preConditions:
			 * - redoInvoker is callable as redoInvoker(ArgsT)
			 * - undoInvoker is callable as undoInvoker(ArgsT)
			 *
			 * postConditions:
			 * - redoInvoker will have been invoked once
			 * - possibleUndoCount increases by one
			 * - previousCommandInfos.back() will be equal to commandInfo
			 * - calling undo() immediately after push will invoke the pushed undoInvoker
			 * - canUndo will be true
			 * - canRedo will be false
			 */
			void push(CommandInfo commandInfo, Args args, RedoInvoker redoInvoker, UndoInvoker undoInvoker)
			{
				push_impl(Command{std::move(commandInfo.get()), std::move(args.get()), std::move(redoInvoker.get()), std::move(undoInvoker.get())});
			}

			/*
			 * preConditions:
			 * - canRedo is true
			 *
			 * postConditions:
			 * - canUndo will be true
			 * - possibleRedoCount -= 1
			 * - possibleUndoCount += 1
			 * - back() of futureCommandInfos will have been moved to back() of previousCommandInfos
			 * - redoInvoker of back of futureCommandInfos will have been invoked once
			 */
			void redo()
			{
				assert(canRedo());

				auto action = futureCommands_.top();
				futureCommands_.pop();
				action.redo();
				previousCommands_.emplace(action);
			}

			/*
			 * preConditions:
			 * - canUndo is true
			 *
			 * postConditions:
			 * - canRedo will be true
			 * - possibleRedoCount += 1
			 * - possibleUndoCount -= 1
			 * - back() of previousCommandInfos will have been moved to back() of futureCommandInfos
			 * - undoInvoker of back of futureCommandInfos will have been invoked once
			 */
			void undo()
			{
				assert(canUndo());
				auto action = previousCommands_.top();
				previousCommands_.pop();
				action.undo();
				futureCommands_.emplace(action);
			}

			/*
			 * postConditions:
			 * - canUndo false
			 * - canRedo false
			 * - possibleRedoCount 0
			 * - possibleUndoCount 0
			 * - previousCommandInfos empty
			 * - futureCommandInfos empty
			 */
			void clear()
			{
				previousCommands_.clear();
				futureCommands_.clear();
			}

			/*
			 * preConditions:
			 * count <= possibleUndoCount
			 *
			 * postConditions:
			 * - canUndo true if count > 0, else false
			 * - canRedo false
			 * - possibleRedoCount 0'
			 * - possibleUndoCount 'count'
			 * - previousCommandInfos keeps only latest 'count'
			 * - futureCommandInfos empty
			 */
			void clear(KeepOnlyLatest count)
			{
				assert(count.get() <= previousCommands_.size());
				if(count.get() == 0)
				{
					clear();
					return;
				}

				futureCommands_.clear();
				if(count.get() == previousCommands_.size()) return;

				previousCommands_.removeFromBottom(previousCommands_.size() - count.get());
			}

			/*
			 * true if possibleRedoCount > 0
			 */
			bool canRedo() const
			{
				return !futureCommands_.empty();
			}

			/*
			 * true if possibleUndoCount > 0
			 */
			bool canUndo() const
			{
				return !previousCommands_.empty();
			}

			std::size_t possibleRedoCount() const
			{
				return futureCommands_.size();
			}
			std::size_t possibleUndoCount() const
			{
				return previousCommands_.size();
			}

			/*
			 * previousCommandInfos().size() == possibleUndoCount()
			 */
			std::vector<CommandInfoT> previousCommandInfos() const
			{
				auto getInfo = [](const Command& action)
				{
					return action.commandInfo();
				};
				return previousCommands_.vec()|ranges::view::transform(getInfo);
			}

			/*
			 * futureCommandInfos().size() == possibleRedoCount()
			 */
			std::vector<CommandInfoT> futureCommandInfos() const
			{
				auto getInfo = [](const Command& action)
				{
					return action.commandInfo();
				};
				return futureCommands_.vec() | ranges::view::transform(getInfo) | ranges::view::reverse;
			}

		private:
			void push_impl(Command action)
			{
				futureCommands_.clear();

				futureCommands_.emplace(std::move(action));
				redo();

			}

			internal::Stack<Command> previousCommands_;
			internal::Stack<Command> futureCommands_;
		};
	}
}