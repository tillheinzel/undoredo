#pragma once
#include <stack>
#include <vector>

namespace th
{
	namespace cmd
	{
		namespace internal
		{
			template<class T>
			class Stack: public std::stack<T, std::vector<T>>
			{
				using Base = std::stack<T, std::vector<T>>;
			public:
				void clear()
				{
					Base::c.clear();
				}

				auto begin() const
				{
					return Base::c.begin();
				}

				auto end() const
				{
					return Base::c.end();
				}

				const std::vector<T>& vec() const
				{
					return Base::c;
				}

				void removeFromBottom(std::size_t numberToRemove)
				{
					std::vector<T>(Base::c.begin() + numberToRemove, Base::c.end()).swap(Base::c);
				}
			};
		}
	}
}