#pragma once
#include <type_traits>

namespace th
{
	namespace cmd
	{
		namespace internal
		{
			template<class T, class ID>
			class StrongType
			{
			public:
				explicit constexpr StrongType(T const& value) : val_(value)
				{}
				template<typename T_ = T, typename = std::enable_if_t<!std::is_reference_v<T_>>>
				explicit constexpr StrongType(T&& value) : val_(std::move(value))
				{}

				constexpr T& get() &
				{
					return val_;
				}
				constexpr T const& get() const&
				{
					return val_;
				}
				T&& get() &&
				{
					return std::move(val_);
				}

			private:
				T val_;
			};
		}
	}
}
