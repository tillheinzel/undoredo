file(GLOB_RECURSE SOURCES *.h *.cpp)

add_executable(testing ${SOURCES} )

target_link_libraries(testing ${PROJECT_NAME})
target_link_libraries(testing debug gtestd debug gtest_maind)
target_link_libraries(testing optimized gtest optimized gtest_main)
