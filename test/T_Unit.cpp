
#include <gtest/gtest.h>

#include <th/cmd.hpp>

namespace cmd = th::cmd;

using CommandInfoT = std::string;
using ArgsT = std::map<std::string, std::string>;
using RedoInvokerT = std::function<void(ArgsT)>;
using UndoInvokerT = std::function<void(ArgsT)>;

using CommandStack = cmd::CommandStack<CommandInfoT, ArgsT, RedoInvokerT, UndoInvokerT>;

using CommandInfo = CommandStack::CommandInfo;
using Args = CommandStack::Args;
using RedoInvoker = CommandStack::RedoInvoker;
using UndoInvoker = CommandStack::UndoInvoker;
using KeepOnlyLatest = CommandStack::KeepOnlyLatest;

auto makeCounterFunction(std::size_t& countingVariable)
{
	return [&count = countingVariable](const auto&) {++count; };
}

class CommandStackFixture : public ::testing::Test
{
public:
	CommandStack commandStack{};

	std::size_t initialRedo1_counter = 0;
	std::size_t initialUndo1_counter = 0;
	std::size_t initialRedo2_counter = 0;
	std::size_t initialUndo2_counter = 0;

	std::size_t redo1_counter = 0;
	std::size_t redo2_counter = 0;
	std::size_t undo1_counter = 0;
	std::size_t undo2_counter = 0;

	std::size_t initialRedoCount = 0u;
	std::size_t initialUndoCount = 0u;

	RedoInvokerT redo1 = makeCounterFunction(redo1_counter);
	RedoInvokerT redo2 = makeCounterFunction(redo2_counter);
	RedoInvokerT undo1 = makeCounterFunction(undo1_counter);
	RedoInvokerT undo2 = makeCounterFunction(undo2_counter);

	ArgsT defaultArgs = {{"",""}};

	void assertRedoCount(std::size_t newCount) const
	{
		EXPECT_EQ(commandStack.possibleRedoCount(), newCount);

		if(newCount >0) EXPECT_TRUE(commandStack.canRedo());
		else EXPECT_FALSE(commandStack.canRedo());

		EXPECT_EQ(commandStack.futureCommandInfos().size(), newCount);
	}
	void assertUndoCount(std::size_t newCount) const
	{
		EXPECT_EQ(commandStack.possibleUndoCount(), newCount);

		if(newCount > 0) EXPECT_TRUE(commandStack.canUndo());
		else EXPECT_FALSE(commandStack.canUndo());

		EXPECT_EQ(commandStack.previousCommandInfos().size(), newCount);
	}

	void assertNoInvokes() const
	{
		EXPECT_EQ(redo1_counter, initialRedo1_counter);
		EXPECT_EQ(undo1_counter, initialUndo1_counter);

		EXPECT_EQ(redo2_counter, initialRedo2_counter);
		EXPECT_EQ(undo2_counter, initialUndo2_counter);
	}

	void assertStackCleared() const
	{
		EXPECT_FALSE(commandStack.canRedo());
		EXPECT_FALSE(commandStack.canUndo());

		EXPECT_EQ(commandStack.possibleRedoCount(), 0u);
		EXPECT_EQ(commandStack.possibleUndoCount(), 0u);

		EXPECT_TRUE(commandStack.previousCommandInfos().empty());
		EXPECT_TRUE(commandStack.futureCommandInfos().empty());
	}

	void assertClearedWithKeep(std::size_t toKeep) const
	{
		EXPECT_FALSE(commandStack.canRedo());

		if(toKeep > 0) EXPECT_TRUE(commandStack.canUndo());
		else EXPECT_FALSE(commandStack.canUndo());

		EXPECT_EQ(commandStack.possibleRedoCount(), 0u);
		EXPECT_EQ(commandStack.possibleUndoCount(), toKeep);

		EXPECT_TRUE(commandStack.futureCommandInfos().empty());
		EXPECT_EQ(commandStack.previousCommandInfos().size(), toKeep);
	}

	void assertOnlyInvoked(const std::string& id) const
	{
		auto pick = [&id](const std::string& checkFor, const auto count)
		{
			return id == checkFor ? count + 1 : count;
		};

		EXPECT_EQ(redo1_counter, pick("redo1", initialRedo1_counter));
		EXPECT_EQ(undo1_counter, pick("undo1", initialUndo1_counter));

		EXPECT_EQ(redo2_counter, pick("redo2", initialRedo2_counter));
		EXPECT_EQ(undo2_counter, pick("undo2", initialUndo2_counter));
		

	}

	void assertPushed(const CommandInfoT& commandInfo) const
	{
		EXPECT_FALSE(commandStack.canRedo());
		EXPECT_TRUE(commandStack.canUndo());

		EXPECT_EQ(commandStack.possibleRedoCount(), 0u);
		EXPECT_EQ(commandStack.possibleUndoCount(), initialUndoCount + 1);

		EXPECT_FALSE(commandStack.previousCommandInfos().empty());
		EXPECT_TRUE(commandStack.futureCommandInfos().empty());

		EXPECT_EQ(commandStack.previousCommandInfos().back(), commandInfo);
	}

	void assertInitialState() const
	{
		EXPECT_EQ(redo1_counter, initialRedo1_counter);
		EXPECT_EQ(undo1_counter, initialUndo1_counter);

		EXPECT_EQ(redo2_counter, initialRedo2_counter);
		EXPECT_EQ(undo2_counter, initialUndo2_counter);

		EXPECT_EQ(commandStack.possibleRedoCount(), initialRedoCount);
		EXPECT_EQ(commandStack.possibleUndoCount(), initialUndoCount);

	}
};

class CommandStackEmpty : public CommandStackFixture
{
public:
};

TEST_F(CommandStackEmpty, correctState)
{
	assertInitialState();

	EXPECT_FALSE(commandStack.canRedo());
	EXPECT_FALSE(commandStack.canUndo());

	EXPECT_TRUE(commandStack.previousCommandInfos().empty());
	EXPECT_TRUE(commandStack.futureCommandInfos().empty());
}

TEST_F(CommandStackEmpty, clear)
{
	commandStack.clear();

	assertNoInvokes();
	assertStackCleared();
}

TEST_F(CommandStackEmpty, clearKeepAll)
{
	const auto toKeep = commandStack.possibleUndoCount();

	commandStack.clear(KeepOnlyLatest(toKeep));

	assertNoInvokes();
	assertClearedWithKeep(toKeep);
}

TEST_F(CommandStackEmpty, clearKeepMore)
{
	EXPECT_DEATH(commandStack.clear(KeepOnlyLatest(commandStack.possibleUndoCount() + 1)), ".*");
}

TEST_F(CommandStackEmpty, push)
{
	auto commandInfo = CommandInfoT{"1"};
	commandStack.push(CommandInfo{commandInfo}, Args{defaultArgs}, RedoInvoker{redo1}, UndoInvoker{undo1});

	assertOnlyInvoked("redo1");
	assertPushed(commandInfo);
}

TEST_F(CommandStackEmpty, undo)
{
	EXPECT_DEATH(commandStack.undo(), ".*");
}

TEST_F(CommandStackEmpty, redo)
{
	EXPECT_DEATH(commandStack.redo(), ".*");
}

class CommandStackMultipleCommands : public CommandStackFixture
{
public:
	CommandStackMultipleCommands()
	{
		commandStack.push(CommandInfo{commandInfo1}, Args{defaultArgs}, RedoInvoker{redo1}, UndoInvoker{undo1});

		commandStack.push(CommandInfo{commandInfo2}, Args{defaultArgs}, RedoInvoker{redo2}, UndoInvoker{undo2});

		initialRedo1_counter = redo1_counter;
		initialUndo1_counter = undo1_counter;
		initialRedo2_counter = redo2_counter;
		initialUndo2_counter = undo2_counter;

		initialRedoCount = commandStack.possibleRedoCount();
		initialUndoCount = commandStack.possibleUndoCount();
	}

	CommandInfoT commandInfo1{"1"};
	CommandInfoT commandInfo2{"2"};
};


TEST_F(CommandStackMultipleCommands, correctState)
{
	assertInitialState();

	EXPECT_FALSE(commandStack.canRedo());
	EXPECT_TRUE(commandStack.canUndo());

	EXPECT_FALSE(commandStack.previousCommandInfos().empty());
	EXPECT_TRUE(commandStack.futureCommandInfos().empty());

	EXPECT_EQ(commandStack.previousCommandInfos().at(0), commandInfo1);
	EXPECT_EQ(commandStack.previousCommandInfos().at(1), commandInfo2);
}

TEST_F(CommandStackMultipleCommands, clear)
{
	commandStack.clear();

	assertNoInvokes();
	assertStackCleared();
}

TEST_F(CommandStackMultipleCommands, clearKeepLess)
{
	const auto toKeep = commandStack.possibleUndoCount()-1;

	commandStack.clear(KeepOnlyLatest(toKeep));

	assertNoInvokes();
	assertClearedWithKeep(toKeep);
}

TEST_F(CommandStackMultipleCommands, clearKeepAll)
{
	const auto toKeep = commandStack.possibleUndoCount();

	commandStack.clear(KeepOnlyLatest(toKeep));

	assertNoInvokes();
	assertClearedWithKeep(toKeep);
}

TEST_F(CommandStackMultipleCommands, clearKeepMore)
{
	EXPECT_DEATH(commandStack.clear(KeepOnlyLatest(commandStack.possibleUndoCount() + 1)), ".*");
}

TEST_F(CommandStackMultipleCommands, push)
{
	auto commandInfo = CommandInfoT{"1"};
	commandStack.push(CommandInfo{commandInfo}, Args{defaultArgs}, RedoInvoker{redo1}, UndoInvoker{undo1});

	assertOnlyInvoked("redo1");
	assertPushed(commandInfo);
}

TEST_F(CommandStackMultipleCommands, undo)
{
	commandStack.undo();

	assertOnlyInvoked("undo2");

	assertUndoCount(initialUndoCount - 1);
	assertRedoCount(initialRedoCount + 1);

	EXPECT_EQ(commandStack.previousCommandInfos().front(), commandInfo1);
	EXPECT_EQ(commandStack.futureCommandInfos().front(), commandInfo2);

}

TEST_F(CommandStackMultipleCommands, redo)
{
	EXPECT_DEATH(commandStack.redo(), ".*");
}

class CommandStackSomewhereInTheMiddle : public CommandStackFixture
{
public:
	CommandStackSomewhereInTheMiddle()
	{
		commandStack.push(CommandInfo{commandInfo1}, Args{defaultArgs}, RedoInvoker{redo1}, UndoInvoker{undo1});

		commandStack.push(CommandInfo{commandInfo2}, Args{defaultArgs}, RedoInvoker{redo2}, UndoInvoker{undo2});

		commandStack.push(CommandInfo{commandInfo2}, Args{defaultArgs}, RedoInvoker{redo2}, UndoInvoker{undo2});

		commandStack.undo();


		initialRedo1_counter = redo1_counter;
		initialUndo1_counter = undo1_counter;
		initialRedo2_counter = redo2_counter;
		initialUndo2_counter = undo2_counter;

		initialRedoCount = commandStack.possibleRedoCount();
		initialUndoCount = commandStack.possibleUndoCount();
	}

	CommandInfoT commandInfo1{"1"};
	CommandInfoT commandInfo2{"2"};
};

TEST_F(CommandStackSomewhereInTheMiddle, correctState)
{
	assertInitialState();

	EXPECT_TRUE(commandStack.canRedo());
	EXPECT_TRUE(commandStack.canUndo());

	EXPECT_EQ(commandStack.previousCommandInfos().size(), 2u);
	EXPECT_EQ(commandStack.futureCommandInfos().size(), 1u);

	EXPECT_EQ(commandStack.previousCommandInfos().front(), commandInfo1);
	EXPECT_EQ(commandStack.previousCommandInfos().back(), commandInfo2);
	EXPECT_EQ(commandStack.futureCommandInfos().front(), commandInfo2);
}

TEST_F(CommandStackSomewhereInTheMiddle, clear)
{
	commandStack.clear();

	assertNoInvokes();
	assertStackCleared();
}

TEST_F(CommandStackSomewhereInTheMiddle, clearKeepLess)
{
	const auto toKeep = commandStack.possibleUndoCount() - 1;

	commandStack.clear(KeepOnlyLatest(toKeep));

	assertNoInvokes();
	assertClearedWithKeep(toKeep);
}

TEST_F(CommandStackSomewhereInTheMiddle, clearKeepAll)
{
	const auto toKeep = commandStack.possibleUndoCount();

	commandStack.clear(KeepOnlyLatest(toKeep));

	assertNoInvokes();
	assertClearedWithKeep(toKeep);
}

TEST_F(CommandStackSomewhereInTheMiddle, clearKeepMore)
{
	EXPECT_DEATH(commandStack.clear(KeepOnlyLatest(commandStack.possibleUndoCount() + 1)), ".*");
}

TEST_F(CommandStackSomewhereInTheMiddle, push)
{
	auto commandInfo = CommandInfoT{"1"};
	commandStack.push(CommandInfo{commandInfo}, Args{defaultArgs}, RedoInvoker{redo1}, UndoInvoker{undo1});

	assertOnlyInvoked("redo1");
	assertPushed(commandInfo);
}

TEST_F(CommandStackSomewhereInTheMiddle, undo)
{
	commandStack.undo();

	assertOnlyInvoked("undo2");

	assertUndoCount(initialUndoCount - 1);
	assertRedoCount(initialRedoCount + 1);


	EXPECT_EQ(commandStack.previousCommandInfos().front(), commandInfo1);

	EXPECT_EQ(commandStack.futureCommandInfos().front(), commandInfo2);
	EXPECT_EQ(commandStack.futureCommandInfos().back(), commandInfo2);
}

TEST_F(CommandStackSomewhereInTheMiddle, redo)
{
	commandStack.redo();

	assertOnlyInvoked("redo2");

	assertUndoCount(initialUndoCount + 1);
	assertRedoCount(initialRedoCount - 1);

	EXPECT_EQ(commandStack.previousCommandInfos().at(0), commandInfo1);
	EXPECT_EQ(commandStack.previousCommandInfos().at(1), commandInfo2);
	EXPECT_EQ(commandStack.previousCommandInfos().at(2), commandInfo2);
}
